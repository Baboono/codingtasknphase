package com.nphase.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Properties;

import com.nphase.entity.Product;
import com.nphase.entity.ShoppingCart;
import com.nphase.util.ProductUtil;

public class ShoppingCartService {
	

	public static double DISCOUNT_MARGIN = 0.1;
	public static double CATEGORY_MIN_ITEM_COUNT_FOR_DISCOUNT = 3;
	
	//TASK 4
	static {
        try (InputStream input = new FileInputStream("C:\\nphase-coding-challange\\src\\main\\resources\\config.properties")) {
            Properties prop = new Properties();
            prop.load(input);
            DISCOUNT_MARGIN = Double.valueOf(prop.getProperty("nphase.discount_margin"));
            CATEGORY_MIN_ITEM_COUNT_FOR_DISCOUNT = Double.valueOf(prop.getProperty("nphase.category_min_item_count_for_discount"));
        } catch (IOException io) {
            io.printStackTrace();
        }
	}

	// TASK 1
	public BigDecimal calculateTotalPrice(ShoppingCart shoppingCart) {

		return ProductUtil.calculateTotalPriceForProducts(shoppingCart.getProducts());

	}

	// TASK 2
	public BigDecimal calculateTotalPriceIncludingDiscounts(ShoppingCart shoppingCart) {

		BigDecimal totalPriceForProcucts = ProductUtil.calculateTotalPriceForProducts(shoppingCart.getProducts());
		if (shoppingCart.getDiscountedProducts() == null || shoppingCart.getDiscountedProducts().isEmpty()) {
			return totalPriceForProcucts;
		}
		BigDecimal totalPriceForDiscountedProcucts = ProductUtil.calculateTotalPriceForProducts(shoppingCart.getDiscountedProducts());
		if (totalPriceForDiscountedProcucts != null && !BigDecimal.ZERO.equals(totalPriceForDiscountedProcucts)) {
			totalPriceForDiscountedProcucts = totalPriceForDiscountedProcucts.subtract(totalPriceForDiscountedProcucts.multiply(BigDecimal.valueOf(DISCOUNT_MARGIN)));
			return totalPriceForProcucts.add(totalPriceForDiscountedProcucts);
		} else {
			return totalPriceForProcucts;
		}

	}

	// TASK 3
	public BigDecimal calculateTotalPriceIncludingCategoryDiscounts(ShoppingCart shoppingCart) {

		BigDecimal result = BigDecimal.ZERO;

		if (shoppingCart.getProductCategoryMap() == null || shoppingCart.getProductCategoryMap().size() == 0) {
			return result;
		}

		for (String key : shoppingCart.getProductCategoryMap().keySet()) {
			List<Product> productsForCategory = shoppingCart.getProductCategoryMap().get(key);
			if (ProductUtil.getTotalProductCount(productsForCategory) > CATEGORY_MIN_ITEM_COUNT_FOR_DISCOUNT) {
				result = result.add(ProductUtil.calculateTotalPriceForProducts(productsForCategory));
				result = result.subtract(result.multiply(BigDecimal.valueOf(DISCOUNT_MARGIN)));
			} else {
				result = result.add(ProductUtil.calculateTotalPriceForProducts(productsForCategory));
			}
		}
		return result;
	}
}
