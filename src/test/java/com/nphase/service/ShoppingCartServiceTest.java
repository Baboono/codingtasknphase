package com.nphase.service;


import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.nphase.entity.Product;
import com.nphase.entity.ShoppingCart;

public class ShoppingCartServiceTest {
    private final ShoppingCartService service = new ShoppingCartService();

    @Test
    public void calculatesPrice()  {
        ShoppingCart cart = new ShoppingCart(Arrays.asList(
                new Product("Tea", BigDecimal.valueOf(5.0), 2),
                new Product("Coffee", BigDecimal.valueOf(6.5), 1)
        ),false,false);

        BigDecimal result = service.calculateTotalPrice(cart);

        Assertions.assertEquals(BigDecimal.valueOf(16.5),result);
    }
    //TASK 2
    @Test
    public void calculatesPriceWithDiscounts()  {
        ShoppingCart cart = new ShoppingCart(Arrays.asList(
                new Product("Tea", BigDecimal.valueOf(5.0), 5),
                new Product("Coffee", BigDecimal.valueOf(3.5), 3)
        ),true,false);

        BigDecimal result = service.calculateTotalPriceIncludingDiscounts(cart);

        Assertions.assertEquals(BigDecimal.valueOf(33.00).stripTrailingZeros(),result.stripTrailingZeros() );
    }
    
    //TASK 3
    @Test
    public void calculatesPriceWithDiscountsByCategory()  {
        ShoppingCart cart = new ShoppingCart(Arrays.asList(
                new Product("Tea","drinks", BigDecimal.valueOf(5.3), 2),
                new Product("Coffee","drinks", BigDecimal.valueOf(3.5), 2),
                new Product("Cheese","food", BigDecimal.valueOf(8), 2)
        ),false,true);

        BigDecimal result = service.calculateTotalPriceIncludingCategoryDiscounts(cart);

        Assertions.assertEquals(BigDecimal.valueOf(31.84),result );
    }

}
