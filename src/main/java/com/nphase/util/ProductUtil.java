package com.nphase.util;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.nphase.entity.Product;

public class ProductUtil {
	
	public static  BigDecimal calculateTotalPriceForProducts(List<Product> products) {
		
		if(products == null || products.isEmpty()) {
			return BigDecimal.ZERO;
		}
		return products
                .stream()
                .map(product -> product.getPricePerUnit().multiply(BigDecimal.valueOf(product.getQuantity())))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    	 
	}
	
	public static  Map<String,List<Product>> getProductMapByCategory(List<Product> inputProducts) {
		return inputProducts.stream().collect(Collectors.groupingBy(Product::getCategory));		
	}
	public static  int getTotalProductCount(List<Product> inputProducts) {
		return inputProducts.stream().mapToInt(Product::getQuantity).sum();
	}

}
