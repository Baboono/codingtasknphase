package com.nphase.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.nphase.service.ShoppingCartService;
import com.nphase.util.ProductUtil;

public class ShoppingCart {

	private final List<Product> products;
	private final List<Product> discountedProducts;
	private final Map<String, List<Product>> productCategoryMap;

	public ShoppingCart(List<Product> inputProducts, boolean includeDiscounts, boolean sortByCategory) {

		List<Product> productsToBeSet = new ArrayList<Product>();
		List<Product> discountedProducts = new ArrayList<Product>();

		if (inputProducts == null) {
			this.products = null;
			this.discountedProducts = null;
			this.productCategoryMap = null;
			return;
		}
		if (includeDiscounts) {
			sortProductToDiscountedAndNonDiscounted(inputProducts, productsToBeSet, discountedProducts);
			productsToBeSet.removeAll(discountedProducts);
			this.products = productsToBeSet;
			this.discountedProducts = discountedProducts;
		} else {
			this.products = inputProducts;
			this.discountedProducts = null;
		}
		if (sortByCategory) {
			productCategoryMap = ProductUtil.getProductMapByCategory(inputProducts);
		} else {
			this.productCategoryMap = null;
		}

	}

	private void sortProductToDiscountedAndNonDiscounted(List<Product> inputProducts, List<Product> productsToBeSet, List<Product> discountedProducts) {

		for (Product product : inputProducts) {
			if (product == null) {
				continue;
			}
			if (product.getQuantity() > ShoppingCartService.CATEGORY_MIN_ITEM_COUNT_FOR_DISCOUNT) {
				discountedProducts.add(product);
			} else {
				productsToBeSet.add(product);
			}
		}
	}

	public List<Product> getProducts() {
		return products;
	}

	public List<Product> getDiscountedProducts() {
		return discountedProducts;
	}

	public Map<String, List<Product>> getProductCategoryMap() {
		return productCategoryMap;
	}

}
